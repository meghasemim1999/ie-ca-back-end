package ir.ac.ut.ece.ie.runnable;

import ir.ac.ut.ece.ie.domain.RestaurantInfo;
import ir.ac.ut.ece.ie.exception.DuplicateRestaurantException;
import ir.ac.ut.ece.ie.exception.LackOfRestaurantException;
import ir.ac.ut.ece.ie.repository.RestaurantRepository;

import java.sql.SQLException;
import java.util.List;

public class GetFoodPartyRestaurantsList implements Runnable
{
    @Override
    public void run() {
        try {
            RestaurantRepository.getInstance().clearFoodPartyMenu();
            List<RestaurantInfo> foodPartyRestaurants = null;
            foodPartyRestaurants = RestaurantRepository.getInstance().getFoodPartyRestaurantsListFromServer();
            if (foodPartyRestaurants == null)
                return;
            for (RestaurantInfo foodPartyRestaurant : foodPartyRestaurants) {
                foodPartyRestaurant.setFoodPartyMenuFromMenu();
                RestaurantRepository.getInstance().addOrUpdate(foodPartyRestaurant);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}