package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.domain.User;

import java.sql.*;

public class UserRepository extends BasicRepository<User, Integer> implements IUserRepository {



    private static final String COLUMNS = " id, firstname, lastname, phoneNumber, email, password, credit, location ";
    private static final String COLUMNS_WITHOUT_ID = " firstname, lastname, phoneNumber, email, password, credit, location ";
    private static final String TABLE_NAME = "User";

    private static UserRepository instance;

    private UserRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement st = con.createStatement();
        st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
        st.executeUpdate(String.format(
                "CREATE TABLE  %s " +
                        "(" +
                        "id INT AUTO_INCREMENT PRIMARY KEY, " +
                        "firstname TEXT, " +
                        "lastname TEXT, " +
                        "phoneNumber TEXT, " +
                        "email TEXT, " +
                        "password TEXT, " +
                        "credit INT, " +
                        "location TEXT " +
                        ");",
                TABLE_NAME));
//                st.executeUpdate(String.format(
//                "INSERT INTO " + TABLE_NAME +
//                        "(" + COLUMNS_WITHOUT_ID + ")" + " VALUES "+
//                        "( 'اصغر','اصغری', '09123456789', 'asgharkopol@kopols.com', 100000, '0_0' );"));

        st.close();
        con.close();
    }

    public static UserRepository getInstance() throws SQLException {
        if (instance == null)
            instance = new UserRepository();

        return instance;
    }

    @Override
    protected String getGetByIdStatement(Integer id) {
        return "SELECT * FROM " + TABLE_NAME +
                " WHERE id = "+ id.toString() + ";";
    }

    @Override
    protected String getGetAllStatement() {
        return "SELECT * FROM " + TABLE_NAME + ";";
    }

    protected String getGetByEmailStatement(String email) {
        return "SELECT * FROM " + TABLE_NAME + " WHERE email = '" + email + "' ;";
    }

    public boolean existByEmail(String email) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetByEmailStatement(email))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if(resultSet.next())
                    return true;
                return false;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getById query.");
                throw ex;
            }
        }
    }

    public User getByEmail(String email) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetByEmailStatement(email))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if(resultSet.next())
                    return convertResultSetToObject(resultSet);
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getById query.");
                throw ex;
            }
        }
        return null;
    }

    @Override
    protected String getAddOrUpdateStatement(User e) {
        try {
            User result = getById(e.getId());
            if (result == null){
                return "INSERT INTO " + TABLE_NAME +
                        "(" + COLUMNS_WITHOUT_ID + ")" + " VALUES " +
                        "(" +
                        '"' + e.getFirstname() + '"' + "," +
                        '"' + e.getLastname() + '"' + "," +
                        '"' + e.getPhoneNumber() + '"' + "," +
                        '"' + e.getEmail() + '"' + "," +
                        '"' + e.getPassword() + '"' + "," +
                        e.getCredit() + "," +
                        '"' + e.getLocation().getX() + "_" + e.getLocation().getY() + '"' +
                        ");";
            }
            else {
                return "UPDATE " + TABLE_NAME + " SET credit = " + e.getCredit() + " WHERE id = " + e.getId() + ";" ;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getCountStatement() {
        return "SELECT COUNT(*) FROM " + TABLE_NAME;
    }

    @Override
    protected User convertResultSetToObject(ResultSet rs) throws SQLException {
        return  new User(
                rs.getInt(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getString(6),
                rs.getInt(7),
                RepositoryUtilities.convertLocationStringToLocation(rs.getString(8)));
    }

    @Override
    public Integer count() throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getCountStatement())
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                resultSet.next();
                return resultSet.getInt(1);
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getById query.");
                throw ex;
            }
        }
    }


}
