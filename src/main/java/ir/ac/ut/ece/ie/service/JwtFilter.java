package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.domain.User;
import ir.ac.ut.ece.ie.exception.InvalidToken;
import ir.ac.ut.ece.ie.repository.UserRepository;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

public class JwtFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(request.getServletPath().equals("/User") && request.getMethod().equals("POST")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String authHeader = request.getHeader("Authorization");

        if (authHeader == null || authHeader.isEmpty() || !authHeader.contains("bearer")) {
            response.setStatus(401);
            return;
        }

        String token = authHeader.split("\\s+")[1];

        if(!JwtToken.verify(token)){
            response.setStatus(403);
            return;
        }

        Long userId = null;
        try {
            userId = JwtToken.getUserId(token);
            if (userId == null) {
                response.setStatus(403);
                return;
            }
        } catch (InvalidToken | ParseException e) {
            e.printStackTrace();
        }

        User loggedInUser = null;
        try {
            loggedInUser = UserRepository.getInstance().getById(Math.toIntExact(userId));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        request.setAttribute("user", loggedInUser);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}