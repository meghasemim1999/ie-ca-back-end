package ir.ac.ut.ece.ie.repository;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.domain.DiscountedFood;
import ir.ac.ut.ece.ie.domain.RestaurantFoodInfo;
import ir.ac.ut.ece.ie.domain.RestaurantInfo;
import ir.ac.ut.ece.ie.exception.LackOfRestaurantException;
import ir.ac.ut.ece.ie.service.RestaurantListItemViewModel;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RestaurantRepository extends BasicRepository<RestaurantInfo, String> implements IRestaurantRepository {
    private static volatile RestaurantRepository instance;
    private static boolean isInstanceCreationStarted = false;

    private ObjectMapper mapper;

    private static final String RESTAURANTS_COLUMNS = " id, name, logo, location";
    private static final String RESTAURANTS_TABLE_NAME = "Restaurant";

    private static final String REGULAR_FOODS_COLUMNS = " restaurantId, name, description, popularity, price, image, doesExist";
    private static final String REGULAR_FOODS_TABLE_NAME = "RegularFood";

    private static final String DISCOUNTED_FOODS_COLUMNS = " restaurantId, name, description, popularity, image, oldPrice, price, doesExist, count";
    private static final String DISCOUNTED_FOODS_TABLE_NAME = "DiscountedFood";


    private RestaurantRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement st = con.createStatement();
        st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", RESTAURANTS_TABLE_NAME));
        st.executeUpdate(String.format(
                "CREATE TABLE  %s " +
                        "(" +
                        "id VARCHAR(64) PRIMARY KEY, " +
                        "name TEXT, " +
                        "logo TEXT, " +
                        "location TEXT " +
                        ");",
                RESTAURANTS_TABLE_NAME));
        st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", REGULAR_FOODS_TABLE_NAME));
        st.executeUpdate(String.format(
                "CREATE TABLE  %s " +
                        "(" +
                        "id INT AUTO_INCREMENT PRIMARY KEY, " +
                        "restaurantId TEXT, " +
                        "name TEXT, " +
                        "description TEXT, " +
                        "popularity FLOAT, " +
                        "price INT, " +
                        "image TEXT ," +
                        "doesExist TINYINT " +
                        ");",
                REGULAR_FOODS_TABLE_NAME));
        st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", DISCOUNTED_FOODS_TABLE_NAME));
        st.executeUpdate(String.format(
                "CREATE TABLE  %s " +
                        "(" +
                        "id INT AUTO_INCREMENT PRIMARY KEY, " +
                        "restaurantId TEXT, " +
                        "name TEXT, " +
                        "description TEXT, " +
                        "popularity FLOAT, " +
                        "image TEXT ," +
                        "oldPrice INT, " +
                        "price INT, " +
                        "doesExist TINYINT, " +
                        "count INT " +
                        ");",
                DISCOUNTED_FOODS_TABLE_NAME));
//        st.executeUpdate(String.format(
//                "INSERT INTO " + TABLE_NAME +
//                        "(" + COLUMNS_WITHOUT_ID + ")" + " VALUES "+
//                        "( 'اصغر','اصغری', '09123456789', 'asgharkopol@kopols.com', 100000, '0_0' );"));


        st.close();
        con.close();
        mapper = new ObjectMapper();
        List<RestaurantInfo> restaurants = getRestaurantsListFromServer();
        if (!restaurants.isEmpty())
            for (RestaurantInfo restaurant : restaurants){
                restaurant.setRegularMenuFromMenu();
                addOrUpdate(restaurant);
            }
    }


    public static RestaurantRepository getInstance() throws SQLException {
        if(isInstanceCreationStarted) {
            try {
                while (instance == null) {
                    Thread.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if(instance == null) {
            isInstanceCreationStarted = true;
            instance = new RestaurantRepository();
            isInstanceCreationStarted = false;
        }

        return instance;
    }

    private List<RestaurantInfo> getRestaurantsListFromServer()
    {
        HttpResponse<String> response = Unirest.get("http://138.197.181.131:8080/restaurants")
                .header("accept", "application/json")
                .asString();
        try {
//            return mapper.readValue(response.getBody(), new TypeReference<>() {});
            return mapper.readValue(response.getBody(), new TypeReference<List<RestaurantInfo>>() {});

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<RestaurantInfo> getFoodPartyRestaurantsListFromServer()
    {
        HttpResponse<String> response = Unirest.get("http://138.197.181.131:8080/foodparty")
                .header("accept", "application/json")
                .asString();
        try {
            return mapper.readValue(response.getBody(), new TypeReference<List<RestaurantInfo>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected String getGetByIdStatement(String id) {
        return "SELECT * FROM " + RESTAURANTS_TABLE_NAME +
                " WHERE id = '"+ id + "';";
    }

    @Override
    protected String getGetAllStatement() {
        return "SELECT * FROM " + RESTAURANTS_TABLE_NAME + " ;";
    }

    @Override
    protected String getAddOrUpdateStatement(RestaurantInfo e) {
        try {
            getById(e.getId());
        }catch (LackOfRestaurantException ex) {
            String query = "INSERT INTO " + RESTAURANTS_TABLE_NAME +
                    " ( " + RESTAURANTS_COLUMNS + " ) VALUES " +
                    "(" +
                    '"' + e.getId() + '"' + "," +
                    '"' + e.getName() + '"' + "," +
                    '"' + e.getLogo() + '"' + "," +
                    '"' + e.getLocation().getX() + "_" + e.getLocation().getY() + '"' +
                    ");" ;
                    addRegularFoods(e.getId(), e.getRegularMenu());
                    addFoodPartyFoods(e.getId(), e.getFoodPartyMenu());
            return query;
        }
        addFoodPartyFoods(e.getId(), e.getFoodPartyMenu());
        return "";
    }

    private void  addFoodPartyFoods(String restaurantId, List<DiscountedFood> foodPartyMenu){
        List<String> statements = getAddFoodPartyFoodsStatement(restaurantId, foodPartyMenu);
        for (int i = 0; i < statements.size(); i++){
            try (Connection con = ConnectionPool.getConnection();
                 PreparedStatement st = con.prepareStatement(statements.get(i))
            ) {
                st.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.out.println("error in Mapper.addFoodPart query.");
            }
        }
    }

    private void  addRegularFoods(String restaurantId, List<RestaurantFoodInfo> regularMenu){
        List<String> statements = getAddRegularFoodsStatement(restaurantId, regularMenu);
        for (int i = 0; i < statements.size(); i++){
            try (Connection con = ConnectionPool.getConnection();
                 PreparedStatement st = con.prepareStatement(statements.get(i))
            ) {
                st.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.out.println("error in Mapper.addOrUpdate query.");
            }
        }
    }

    private List<String> getAddFoodPartyFoodsStatement(String restaurantId, List<DiscountedFood> foodPartyMenu) {
        List<String> insertStatements = new ArrayList<>();
        if(foodPartyMenu == null){
            return insertStatements;
        }
        for (int i = 0; i < foodPartyMenu.size(); i++) {
            DiscountedFood food = foodPartyMenu.get(i);
            insertStatements.add("INSERT INTO " + DISCOUNTED_FOODS_TABLE_NAME + "(" + DISCOUNTED_FOODS_COLUMNS + ") VALUES ('" +
                                restaurantId + "', '" +
                                food.getName() + "', '" +
                                food.getDescription() + "'," +
                                food.getPopularity() + ",'" +
                                food.getImage() + "', " +
                                food.getOldPrice() + ", " +
                                food.getPrice() + "," +
                                food.getDoesExist() + "," +
                                food.getCount() + ");");
        }
        return insertStatements;
    }

    private List<String> getAddRegularFoodsStatement(String restaurantId, List<RestaurantFoodInfo> regularMenu) {
        List<String> insertStatements = new ArrayList<>();
        if(regularMenu == null){
            return insertStatements;
        }
        for (int i = 0; i < 1; i++) {
            RestaurantFoodInfo food = regularMenu.get(i);
            insertStatements.add("INSERT INTO " + REGULAR_FOODS_TABLE_NAME + " (" + REGULAR_FOODS_COLUMNS + ") " + " VALUES " +
                                "( '" +
                                restaurantId +  "', '" +
                                food.getName() + "', '" +
                                food.getDescription() + "', " +
                                food.getPopularity() + ", " +
                                food.getPrice() + ", '" +
                                food.getImage() + "', " +
                                food.getDoesExist() + ");");
        }
        return insertStatements;
    }

    @Override
    public RestaurantInfo getById(String id) throws LackOfRestaurantException {
        RestaurantInfo restaurantInfo = null;
        try {
            restaurantInfo = super.getById(id);
            if(restaurantInfo == null)
                throw new LackOfRestaurantException("Restaurant with id: " + id + " does not exist.");
        } catch (SQLException throwables) {
            // TODO exception handling
        }
        return restaurantInfo;
    }

    @Override
    protected RestaurantInfo convertResultSetToObject(ResultSet rs) throws SQLException {
        return new RestaurantInfo
                (
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        RepositoryUtilities.convertLocationStringToLocation(rs.getString(4)),
                        getRegularMenuByRestaurantId(rs.getString(1), rs.getString(2)),
                        getFoodPartyMenuByRestaurantId(rs.getString(1), rs.getString(2))
                );
    }

    @Override
    public String getClearFoodPartyMenuStatement(){
        return "TRUNCATE " + DISCOUNTED_FOODS_TABLE_NAME + " ;";
    }

    @Override
    public void clearFoodPartyMenu() throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getClearFoodPartyMenuStatement())
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.clearFoodPartyMenu query.");
                throw ex;
            }
        }
    }

    String getGetFoodPartyMenuByRestaurantIdStatement(String restaurantId) {
        return "SELECT * FROM " + DISCOUNTED_FOODS_TABLE_NAME +
                " WHERE restaurantId = '" + restaurantId + "' ;";
    }

    String getGetRegularMenuByRestaurantIdStatement(String restaurantId) {
        return "SELECT * FROM " + REGULAR_FOODS_TABLE_NAME +
                " WHERE restaurantId = '" + restaurantId + "' ;";
    }

    private List<DiscountedFood> getFoodPartyMenuByRestaurantId(String restaurantId, String restaurantName) throws SQLException {
        List<DiscountedFood> foods = new ArrayList<>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetFoodPartyMenuByRestaurantIdStatement(restaurantId))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while(resultSet.next()){
                    foods.add(convertResultSetToDiscountedFood(resultSet, restaurantName));
                }
                return foods;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
    }

    private DiscountedFood convertResultSetToDiscountedFood(ResultSet rs, String restaurantName) throws SQLException {
        return new DiscountedFood(
                    rs.getString(3),
                    restaurantName,
                    rs.getString(4),
                    rs.getFloat(5),
                    rs.getString(6),
                    rs.getInt(7),
                    rs.getInt(8),
                    rs.getInt(9)
                );
    }

    private List<RestaurantFoodInfo> getRegularMenuByRestaurantId(String restaurantId, String restaurantName) throws SQLException {
        List<RestaurantFoodInfo> foods = new ArrayList<>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetRegularMenuByRestaurantIdStatement(restaurantId))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while(resultSet.next()){
                    foods.add(convertResultSetToRegularFood(resultSet, restaurantName));
                }
                return foods;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getRegularMenuByRestaurant query.");
                throw ex;
            }
        }
    }

    private RestaurantFoodInfo convertResultSetToRegularFood(ResultSet rs, String restaurantName) throws SQLException {
        return new RestaurantFoodInfo(
                rs.getString(3),
                restaurantName,
                rs.getString(4),
                rs.getFloat(5),
                rs.getString(7),
                rs.getInt(6)
        );
    }

    @Override
    public String getGetByNameStatement(String name) {
        return "SELECT * FROM " + RESTAURANTS_TABLE_NAME +
                " WHERE name = '" + name + "' ;";
    }

    public String getSearchByRestaurantStatement(String searchString) {
        return "SELECT id FROM " + RESTAURANTS_TABLE_NAME +
                " WHERE name LIKE '%" + searchString + "%' ;";
    }

    @Override
    public List<RestaurantListItemViewModel> searchByRestaurant(String searchString) throws SQLException {
        List<RestaurantListItemViewModel> restaurants = new ArrayList<>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getSearchByRestaurantStatement(searchString))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while(resultSet.next()){
                    restaurants.add(convertRestaurantInfoToRestaurantListItemViewModel(getById(resultSet.getString(1))));
                }
                return restaurants;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
    }

    private RestaurantListItemViewModel convertRestaurantInfoToRestaurantListItemViewModel(RestaurantInfo restaurantInfo) {
        RestaurantListItemViewModel restaurantViewModel = new RestaurantListItemViewModel();
        restaurantViewModel.id = restaurantInfo.getId();
        restaurantViewModel.name = restaurantInfo.getName();
        restaurantViewModel.logo = restaurantInfo.getLogo();
        return restaurantViewModel;
    }

    public String getSearchByFoodInRegularFoodsStatement(String searchString) {
        return "SELECT restaurantId FROM " + REGULAR_FOODS_TABLE_NAME +
                " WHERE name LIKE '%" + searchString + "%' ;";
    }

    public String getSearchByFoodInDiscountedFoodsStatement(String searchString) {
        return "SELECT restaurantId FROM " + DISCOUNTED_FOODS_TABLE_NAME +
                " WHERE name LIKE '%" + searchString + "%' ;";
    }

    @Override
    public List<RestaurantListItemViewModel> searchByFood(String searchString) throws SQLException {
        List<RestaurantListItemViewModel> restaurants = new ArrayList<>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getSearchByFoodInRegularFoodsStatement(searchString))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while(resultSet.next()){
                    restaurants.add(convertRestaurantInfoToRestaurantListItemViewModel(getById(resultSet.getString(1))));
                }
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getSearchByFoodInDiscountedFoodsStatement(searchString))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while(resultSet.next()){
                    restaurants.add(convertRestaurantInfoToRestaurantListItemViewModel(getById(resultSet.getString(1))));
                }
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
        return restaurants;
    }

    @Override
    public RestaurantInfo getByName(String name) throws LackOfRestaurantException, SQLException {
        RestaurantInfo result;
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetByNameStatement(name))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if(resultSet.next())
                    return convertResultSetToObject(resultSet);
                return null;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getByName query.");
                throw ex;
            }
        }
    }

}
