package ir.ac.ut.ece.ie.exception;

public class NotEnoughCreditException extends Exception {
    public NotEnoughCreditException(String message) {
        super(message);
    }
}
