package ir.ac.ut.ece.ie.domain;

public class SearchRestaurantInfo {
    private String name;

    public SearchRestaurantInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
