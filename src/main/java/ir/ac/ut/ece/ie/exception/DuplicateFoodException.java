package ir.ac.ut.ece.ie.exception;

public class DuplicateFoodException extends Exception {
    public DuplicateFoodException(String message) {
        super(message);
    }
}
