package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.domain.LoghmehCore;
import ir.ac.ut.ece.ie.domain.User;
import ir.ac.ut.ece.ie.exception.LackOfUserException;
import ir.ac.ut.ece.ie.exception.NoLoggedInUser;
import ir.ac.ut.ece.ie.repository.UserRepository;
import ir.ac.ut.ece.ie.service.requestBody.IncreaseCreditRequestBody;
import ir.ac.ut.ece.ie.service.requestBody.SignUpRequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.sql.SQLException;

@RestController
public class UserService {
    @RequestMapping(value = "/User", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult SignUp(@RequestBody SignUpRequestBody body) {
        try {
            if(UserRepository.getInstance().existByEmail(body.getEmail()))
                return new ServiceResult(false, "Email already exists", null);
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            User newUser = new User(-1, body.getFirstname(), body.getLastname(), body.getPhoneNumber(), body.getEmail(), encoder.encode(body.getPassword()), User.INITIAL_CREDIT, User.INITIAL_LOCATION);
            UserRepository.getInstance().addOrUpdate(newUser);
        } catch (SQLException exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, null,null);
    }

    @RequestMapping(value = "/User/Credit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult IncreaseCredit(@RequestBody IncreaseCreditRequestBody body, @RequestAttribute("user") User loggedInUser) {
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            LoghmehCore.getInstance().increaseCredit(body.getExtraCredit());
        } catch (SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, null,null);
    }

    @RequestMapping(value = "/User", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getUserProfile(@RequestAttribute("user") User loggedInUser) {
        User user = null;
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            user = LoghmehCore.getInstance().getUserProfile();
        } catch (LackOfUserException | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, user, null);
    }

}
