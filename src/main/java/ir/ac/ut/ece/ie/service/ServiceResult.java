package ir.ac.ut.ece.ie.service;

public class ServiceResult {
    private boolean successful;
    private Object result;
    private Exception exception;

    public ServiceResult(boolean successful, Object result, Exception exception)
    {
        this.successful = successful;
        this.result = result;
        this.exception = exception;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
