package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.domain.LoghmehCore;
import ir.ac.ut.ece.ie.domain.RestaurantInfo;
import ir.ac.ut.ece.ie.domain.User;
import ir.ac.ut.ece.ie.exception.LackOfUserException;
import ir.ac.ut.ece.ie.repository.RestaurantRepository;
import ir.ac.ut.ece.ie.service.requestBody.IncreaseCreditRequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
public class SearchService {
    @RequestMapping(value = "/Search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getUserProfile(@RequestParam(value = "searchString") String searchString, @RequestParam(value = "searchFrom") String searchFrom) {
        List<RestaurantListItemViewModel> restaurants = null;
        System.out.printf(searchString + " " + searchFrom);
        try {
            if(searchFrom.equals("R"))
                restaurants = RestaurantRepository.getInstance().searchByRestaurant(searchString);
            else if (searchFrom.equals("F"))
                restaurants = RestaurantRepository.getInstance().searchByFood(searchString);
        } catch (SQLException exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, restaurants, null);
    }

}
