package ir.ac.ut.ece.ie.service;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;
import java.io.IOException;

public class DelayFilter implements Filter {
    private static int DELAY_AMOUNT_MILLISECOND = 0;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        try {
            Thread.sleep(DELAY_AMOUNT_MILLISECOND);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        HttpServletResponse response = (HttpServletResponse) servletResponse;

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
