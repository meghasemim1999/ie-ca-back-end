package ir.ac.ut.ece.ie.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

abstract public class BasicRepository<Entity, ID> implements IRepository<Entity, ID> {

    abstract protected String getGetByIdStatement(ID id);

    abstract protected String getGetAllStatement();

    abstract protected String getAddOrUpdateStatement(Entity e) throws SQLException;

    abstract protected Entity convertResultSetToObject(ResultSet rs) throws SQLException;

    public Entity getById(ID id) throws SQLException {
        Entity result;
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetByIdStatement(id))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if(resultSet.next())
                    return convertResultSetToObject(resultSet);
                return null;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getById query.");
                throw ex;
            }
        }
    }

    public List<Entity> getAll() throws SQLException{
        List<Entity> entities = new ArrayList<Entity>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetAllStatement())
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while(resultSet.next()){
                    entities.add(convertResultSetToObject(resultSet));
                }
                return entities;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
    }

    public void addOrUpdate(Entity entity) throws SQLException{
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getAddOrUpdateStatement(entity))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.addOrUpdate query.");
                throw ex;
            }
        }
    }
}
