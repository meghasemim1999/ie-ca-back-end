package ir.ac.ut.ece.ie.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.domain.Delivery;
import ir.ac.ut.ece.ie.domain.Location;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeliveryRepository {
    private static DeliveryRepository instance;

    private List<Delivery> deliveries;

    private ObjectMapper mapper;

    private DeliveryRepository() {
        deliveries = new ArrayList<>();
        mapper = new ObjectMapper();
    }

    public static DeliveryRepository getInstance(){
        if(instance == null)
            instance = new DeliveryRepository();

        return instance;
    }

    private List<Delivery> getDeliveriesListFromServer()
    {
        HttpResponse<String> response = Unirest.get("http://138.197.181.131:8080/deliveries")
                .header("accept", "application/json")
                .asString();
        try {
            return mapper.readValue(response.getBody(), new TypeReference<List<Delivery>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Delivery getById(String id){
        for (Delivery delivery : deliveries){
            if(delivery.getId().equals(id))
                return delivery;
        }
        return null;
    }

    public void addOrUpdate(Delivery newDelivery) {
        for (int i = 0; i < deliveries.size(); i++) {
            Delivery delivery = deliveries.get(i);
            if (delivery.getId().equals(newDelivery.getId())) {
                deliveries.set(i, newDelivery);
                return;
            }
        }
        deliveries.add(newDelivery);
    }

    public Delivery searchForBestDelivery(Location userLocation, Location restaurantLocation){
        List<Delivery> availableDeliveries = DeliveryRepository.getInstance().getDeliveriesListFromServer();
        double minTimeToDeliver = Double.MAX_VALUE , neededTime;
        int bestDeliveryIndex = -1;
        if (availableDeliveries != null) {
            boolean deliveryAssigned = false;
            for (int i = 0; i < availableDeliveries.size(); i++) {
                neededTime = availableDeliveries.get(i).neededTimeToDeliver(userLocation, restaurantLocation);
                if (minTimeToDeliver > neededTime) {
                    bestDeliveryIndex = i;
                    minTimeToDeliver = neededTime;
                    deliveryAssigned = true;
                }
            }
            if(deliveryAssigned){
                Delivery bestExistingDelivery = getById(availableDeliveries.get(bestDeliveryIndex).getId());
                if(bestExistingDelivery == null)
                    return availableDeliveries.get(bestDeliveryIndex);
                else
                    return bestExistingDelivery;
            }
        }

        return null;
    }
}
