package ir.ac.ut.ece.ie.service.requestBody;

public class IntendedFoodRequestBody {
    private String restaurantName;
    private String foodName;
    private String isRegular;

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getIsRegular() {
        return isRegular;
    }

    public void setIsRegular(String regular) {
        isRegular = regular;
    }
}
