package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.domain.*;
import ir.ac.ut.ece.ie.exception.LackOfOrderException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderRepository extends BasicRepository<Order, Integer> implements IOrderRepository {
    private static OrderRepository instance;

    private static final String ORDER_COLUMNS_WITHOUT_ID = " state, restaurantId, userId ";
    private static final String ORDER_TABLE_NAME = "Orders";

    private static final String ORDERED_FOOD_COLUMNS_WITHOUT_ID = " orderId, foodId, isRegular, number";
    private static final String ORDERED_FOOD_TABLE_NAME = "OrderedFood";



    private OrderRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement st = con.createStatement();
        st.executeUpdate(String.format("DROP TABLE IF EXISTS %s ;", ORDER_TABLE_NAME));
        st.executeUpdate(String.format(
                "CREATE TABLE  %s " +
                        "(" +
                        "id INT AUTO_INCREMENT PRIMARY KEY, " +
                        "state TEXT, " +
                        "restaurantId TEXT, " +
                        "userId INT " +
                        ");",
                ORDER_TABLE_NAME));
        st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", ORDERED_FOOD_TABLE_NAME));
        st.executeUpdate(String.format(
                "CREATE TABLE  %s " +
                        "(" +
                        "id INT AUTO_INCREMENT PRIMARY KEY, " +
                        "orderId INT, " +
                        "foodId INT, " +
                        "isRegular TINYINT, " +
                        "number INT " +
                        ");",
                ORDERED_FOOD_TABLE_NAME));
        st.close();
        con.close();
    }

    public static OrderRepository getInstance() throws SQLException {
        if (instance == null)
            instance = new OrderRepository();

        return instance;
    }

    @Override
    protected String getGetByIdStatement(Integer id) {
        return "SELECT * FROM " + ORDER_TABLE_NAME +
                " WHERE id = " + id.toString() + ";";
    }

    @Override
    protected String getGetAllStatement() {
        return "SELECT * FROM " + ORDER_TABLE_NAME + ";";
    }

    @Override
    protected Order convertResultSetToObject(ResultSet rs) throws SQLException {
        User orderOwner = UserRepository.getInstance().getById(rs.getInt(4));
        RestaurantInfo restaurant = RestaurantRepository.getInstance().getById(rs.getString(3));
        RestaurantViewModel orderRestaurant = new RestaurantViewModel(restaurant.getId(), restaurant.getLogo(), restaurant.getName(), restaurant.getLocation(), restaurant.getRegularMenu(), restaurant.getFoodPartyMenu());
        List<OrderedFoodInfo> orderedFoods = getOrderedFoods(rs.getInt(1), restaurant.getName());
        Order order = new Order(rs.getInt(1) ,orderedFoods, orderOwner, orderRestaurant);
        order.setOrderState(Order.OrderState.valueOf(rs.getString(2)));
        return order;
    }

    public String getGetOrderedFoodsStatement(Integer orderId) {
        return "SELECT * FROM OrderedFood WHERE id = " + orderId + ";";
    }

    public Order getById(int id) throws LackOfOrderException {
        Order order = null;
        try {
            order = super.getById(id);
            if(order == null)
                throw new LackOfOrderException("Order with id: " + id + " does not exist.");
        } catch (SQLException throwables) {
            // TODO exception handling
        }
        return order;
    }

    @Override
    protected String getAddOrUpdateStatement(Order e) throws SQLException {
        try {
            getById(e.getId());
        }catch (LackOfOrderException ex) {
            String query = "INSERT INTO " + ORDER_TABLE_NAME +
                    " ( " + ORDER_COLUMNS_WITHOUT_ID + " ) VALUES " +
                    "(" +
                    '"' + e.getOrderState().toString() + '"' + "," +
                    '"' + e.getOrderRestaurant().getId() + '"' + "," +
                    e.getOrderOwner().getId() +
                    ");" ;
            addOrderedFoods(e.getId(), e.getOrderedFoods());
            return query;
        }
        return "UPDATE " + ORDER_TABLE_NAME + " SET state = '" + e.getOrderState().toString() + "' WHERE id = " + e.getId() + ";" ;
    }

    private void addOrderedFoods(int orderId, List<OrderedFoodInfo> orderedFoods) throws SQLException {
        List<String> statments = getAddOrderedFoodsStatement(orderId, orderedFoods);
        for (int i = 0; i < statments.size(); i++){
            try (Connection con = ConnectionPool.getConnection();
                 PreparedStatement st = con.prepareStatement(statments.get(i))
            ) {
                st.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.out.println("error in Mapper.addOrUpdate query.");
            }
        }
    }

    private List<String> getAddOrderedFoodsStatement(int orderId, List<OrderedFoodInfo> orderedFoods) throws SQLException {
        List<String> insertStatements = new ArrayList<>();
        for (int i = 0; i < 1; i++) {
            OrderedFoodInfo food = orderedFoods.get(i);
            insertStatements.add("INSERT INTO " + ORDERED_FOOD_TABLE_NAME + " (" + ORDERED_FOOD_COLUMNS_WITHOUT_ID + ") " + " VALUES " +
                    "( " +
                    orderId +  ", " +
                    findFoodId(food.isRegular(), food.getFoodName(), food.getRestaurantName()) + ", " +
                    food.isRegular() + ", " +
                    food.getOrdersNumber() +
                    ");");
        }
        return insertStatements;
//        foodId, isRegular, number
    }

    private Integer findFoodId(boolean isRegular, String foodName, String restaurantName) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getFindFoodIdStatement(isRegular, foodName, restaurantName))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if(resultSet.next()){
                    return resultSet.getInt(1);
                }
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
        return -1;
    }

    private String getFindFoodIdStatement(boolean isRegular, String foodName, String restaurantName) throws SQLException {
        return "SELECT id FROM " + (isRegular ? "RegularFood" : "DiscountedFood") + " WHERE name = '" + foodName + "' AND restaurantId = '" + RestaurantRepository.getInstance().getByName(restaurantName).getId() + "';";
    }

    public List<OrderedFoodInfo> getOrderedFoods(Integer orderId, String restaurantName) throws SQLException {
        List<OrderedFoodInfo> foods = new ArrayList<>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetOrderedFoodsStatement(orderId))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while(resultSet.next()){
                    foods.add(convertResultSetToOrderedFoodInfo(resultSet, restaurantName));
                }
                return foods;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
    }

    private OrderedFoodInfo convertResultSetToOrderedFoodInfo(ResultSet resultSet, String restaurantName) throws SQLException {
        FoodDetailsDAO foodDetails = getFoodDetails(resultSet.getBoolean(4), resultSet.getInt(3));
        return new OrderedFoodInfo(foodDetails.getFoodName(), restaurantName, resultSet.getInt(5), foodDetails.getFoodPrice(), resultSet.getBoolean(4));
    }

    public String getGetFoodDetailsStatement(boolean isRegular, Integer foodId) {
        return "SELECT name, price FROM " + (isRegular ? "RegularFood" : "DiscountedFood") + " WHERE id = " + foodId + ";";
    }

    public FoodDetailsDAO getFoodDetails(boolean isRegular, Integer foodId) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetFoodDetailsStatement(isRegular, foodId))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                resultSet.next();
                FoodDetailsDAO foodDetails = new FoodDetailsDAO(resultSet.getString(1), resultSet.getInt(2));
                return foodDetails;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
    }

    private String getGetUserOrdersStatement(Integer userId) {
        return "SELECT id FROM " + ORDER_TABLE_NAME + " WHERE userId = " + userId + ";";
    }

    public List<Order> getUserOrders(Integer userID) throws SQLException, LackOfOrderException {
        List<Order> orders = new ArrayList<>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getGetUserOrdersStatement(userID))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while (resultSet.next()) {
                    orders.add(getById(resultSet.getInt(1)));
                }
                return orders;
            } catch (SQLException | LackOfOrderException ex) {
                System.out.println("error in Mapper.getAll query.");
                throw ex;
            }
        }
    }
}
