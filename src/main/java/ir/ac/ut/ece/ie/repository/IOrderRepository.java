package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.domain.FoodInfo;
import ir.ac.ut.ece.ie.domain.Order;
import ir.ac.ut.ece.ie.domain.OrderedFoodInfo;
import ir.ac.ut.ece.ie.domain.RestaurantFoodInfo;

import java.sql.SQLException;
import java.util.List;

public interface IOrderRepository extends  IRepository<Order, Integer>{
}