package ir.ac.ut.ece.ie.repository;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<Entity, ID> {
    Entity getById(ID id) throws SQLException;
    List<Entity> getAll() throws SQLException;
    void addOrUpdate(Entity t) throws SQLException;
}

