package ir.ac.ut.ece.ie.domain;

public class DiscountedFood extends RestaurantFoodInfo{
    private int count;
    private int oldPrice;

    public DiscountedFood(String name, String restaurantName, String description, float popularity, String image, int oldPrice, int price, int count)
    {
        super(name, restaurantName, description, popularity, image, price);
        this.oldPrice = oldPrice;
        this.count = count;
    }

    public DiscountedFood(){
    }

    public int getCount() {
        return this.count;
    }

    public int getOldPrice() {
        return oldPrice;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setOldPrice(int oldPrice) {
        this.oldPrice = oldPrice;
    }

    public void decreaseCount(){
        this.count--;
    }
}


