package ir.ac.ut.ece.ie.service.requestBody;

public class GoogleLoginRequestBody {
    private String token;

    public GoogleLoginRequestBody() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
