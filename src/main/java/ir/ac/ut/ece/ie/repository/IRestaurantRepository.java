package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.domain.RestaurantInfo;
import ir.ac.ut.ece.ie.service.RestaurantListItemViewModel;

import java.sql.SQLException;
import java.util.List;

public interface IRestaurantRepository extends IRepository<RestaurantInfo, String> {
    RestaurantInfo getByName(String name) throws SQLException;
    void clearFoodPartyMenu() throws SQLException;
    String getClearFoodPartyMenuStatement();
    String getGetByNameStatement(String name);

    List<RestaurantListItemViewModel> searchByRestaurant(String searchString) throws SQLException;

    List<RestaurantListItemViewModel> searchByFood(String searchString) throws SQLException;
}
