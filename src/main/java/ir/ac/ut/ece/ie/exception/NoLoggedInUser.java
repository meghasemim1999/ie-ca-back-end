package ir.ac.ut.ece.ie.exception;

public class NoLoggedInUser extends Exception{
    public NoLoggedInUser(String message) {
        super(message);
    }
}
