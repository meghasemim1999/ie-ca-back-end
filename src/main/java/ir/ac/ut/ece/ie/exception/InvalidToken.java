package ir.ac.ut.ece.ie.exception;

public class InvalidToken extends Exception {
    public InvalidToken(String message) {
        super(message);
    }
}
