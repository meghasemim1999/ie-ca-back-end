package ir.ac.ut.ece.ie.service;

import java.text.ParseException;
import java.util.Date;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.*;
import com.nimbusds.jwt.*;
import ir.ac.ut.ece.ie.exception.InvalidToken;

public class JwtToken {
    private static final String SECRET_KEY = "loghmeloghmeloghmeloghmeloghmeloghmeloghmeloghme";
    private static final String ISSUER = "ir.ac.ut.ece.ie.loghme";

    public static String create(Integer userId) {
        try {
            JWSSigner signer = null;
            signer = new MACSigner(SECRET_KEY);
            JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                    .issuer(ISSUER)
                    .issueTime(new Date(new Date().getTime()))
                    .expirationTime(new Date(new Date().getTime() + 1000 * 60 * 60 * 24 ))
                    .claim("userId", userId)
                    .build();

            SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);
            signedJWT.sign(signer);
            return signedJWT.serialize();
        } catch (JOSEException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean verify(String jwtToken) {
        try {
            SignedJWT signedJWT = null;
            signedJWT = SignedJWT.parse(jwtToken);
            JWSVerifier verifier = null;
            verifier = new MACVerifier(SECRET_KEY);
            if (!signedJWT.verify(verifier))
                return false;
            if(new Date().before(signedJWT.getJWTClaimsSet().getExpirationTime()))
                return true;
        } catch (ParseException | JOSEException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Long getUserId(String jwtToken) throws InvalidToken, ParseException {
        if(!verify(jwtToken))
            throw new InvalidToken("Jwt Token is not valid");
        SignedJWT signedJWT = null;
        signedJWT = SignedJWT.parse(jwtToken);
        return (Long) signedJWT.getJWTClaimsSet().getClaim("userId");

    }
}