package ir.ac.ut.ece.ie.domain;

import java.util.List;

public class Order {
    private User orderOwner;
    private int id;
    private OrderState orderState;
    private RestaurantViewModel orderRestaurant;
    private List<OrderedFoodInfo> orderedFoods;

    public Order(List<OrderedFoodInfo> orderedFoodInfos, User orderOwner, RestaurantViewModel orderRestaurant)
    {
        this(-1, orderedFoodInfos, orderOwner, orderRestaurant);
    }

    public Order(int id, List<OrderedFoodInfo> orderedFoodInfos, User orderOwner, RestaurantViewModel orderRestaurant)
    {
        this.id = id;
        this.orderedFoods = orderedFoodInfos;
        this.orderOwner = orderOwner;
        this.orderRestaurant = orderRestaurant;
        orderState = OrderState.SEARCHING_FOR_DELIVERY;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public int getId() {
        return id;
    }

    public User getOrderOwner() {
        return orderOwner;
    }

    public RestaurantViewModel getOrderRestaurant() {
        return orderRestaurant;
    }

    public List<OrderedFoodInfo> getOrderedFoods() {
        return orderedFoods;
    }

    public enum OrderState {
        SEARCHING_FOR_DELIVERY,
        DELIVERY_ON_THE_WAY,
        DELIVERED
    }
}
