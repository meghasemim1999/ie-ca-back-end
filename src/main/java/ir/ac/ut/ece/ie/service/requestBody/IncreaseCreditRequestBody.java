package ir.ac.ut.ece.ie.service.requestBody;

public class IncreaseCreditRequestBody {
    private Long extraCredit;

    public Long getExtraCredit() {
        return extraCredit;
    }

    public void setExtraCredit(Long extraCredit) {
        this.extraCredit = extraCredit;
    }
}
