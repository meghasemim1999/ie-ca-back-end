package ir.ac.ut.ece.ie.exception;

public class LackOfAnyRestaurantException extends Exception {
    public LackOfAnyRestaurantException(String message) {
        super(message);
    }
}
