package ir.ac.ut.ece.ie.exception;

public class DuplicateRestaurantException extends Exception {
    public DuplicateRestaurantException(String message) {
        super(message);
    }
}
