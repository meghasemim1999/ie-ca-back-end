package ir.ac.ut.ece.ie.exception;

public class LackOfOrderException extends Exception {
    public LackOfOrderException(String message) {
        super(message);
    }
}
