package ir.ac.ut.ece.ie.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

public class RestaurantInfo {

    private String id;
    private String name;
    private String logo;
    private Location location;

    @JsonProperty("menu")
    private List<JsonNode> menu;

    @JsonIgnore
    private List<RestaurantFoodInfo> regularMenu;

    @JsonIgnore
    private List<DiscountedFood> foodPartyMenu;

    public RestaurantInfo(String id, String name, String logo, Location location, List<RestaurantFoodInfo> regularMenu, List<DiscountedFood> foodPartyMenu) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.location = location;
        this.regularMenu = regularMenu;
        this.foodPartyMenu = foodPartyMenu;
    }

    public RestaurantInfo(){
    }

    public void clearFoodPartyMenu(){
        this.foodPartyMenu = null;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setFoodPartyMenu(List<DiscountedFood> foodPartyMenu){
        this.foodPartyMenu = foodPartyMenu;
    }

    public void setFoodPartyMenuFromMenu() {
        foodPartyMenu = new ArrayList<>();
        for (JsonNode jsonNode : menu)
        {
            DiscountedFood  discountedFood = new DiscountedFood();
            discountedFood.setName(jsonNode.get("name").asText());
            discountedFood.setRestaurantName(this.name);
            discountedFood.setDescription(jsonNode.get("description").asText());
            discountedFood.setPopularity((float) jsonNode.get("popularity").asDouble());
            discountedFood.setPrice(jsonNode.get("price").asInt());
            discountedFood.setImage(jsonNode.get("image").asText());
            discountedFood.setCount(jsonNode.get("count").asInt());
            discountedFood.setOldPrice(jsonNode.get("oldPrice").asInt());
            foodPartyMenu.add(discountedFood);
        }
        menu = null;
    }

    public List<DiscountedFood>  getFoodPartyMenu() {
        return this.foodPartyMenu;
    }

    public void setRegularMenu(List<RestaurantFoodInfo> regularMenu){
        this.regularMenu = regularMenu;
    }

    public void setRegularMenuFromMenu() {
        if(menu == null)
            return;

        regularMenu = new ArrayList<>();
        for (JsonNode jsonNode : menu)
        {
            RestaurantFoodInfo food = new RestaurantFoodInfo();
            food.setName(jsonNode.get("name").asText());
            food.setRestaurantName(this.name);
            food.setDescription(jsonNode.get("description").asText());
            food.setPopularity((float) jsonNode.get("popularity").asDouble());
            food.setPrice(jsonNode.get("price").asInt());
            food.setImage(jsonNode.get("image").asText());

            regularMenu.add(food);
        }
        menu = null;
    }

    public List<RestaurantFoodInfo> getRegularMenu() {
        return this.regularMenu;
    }

    public boolean hasFoodParty() {
        return ((foodPartyMenu != null) && !foodPartyMenu.isEmpty());
    }
}
