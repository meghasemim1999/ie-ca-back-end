package ir.ac.ut.ece.ie.domain;

public class User {
    final public static Integer INITIAL_CREDIT = 0;
    final public static Location INITIAL_LOCATION = new Location(0,0);

    private int id;
    private String firstname;
    private String lastname;
    private String phoneNumber;
    private String email;
    private String password;
    private long credit;
    private Location location;

    public User(int id, String firstname, String lastname, String phoneNumber, String email, String password, long credit, Location location) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.credit = credit;
        this.location = location;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public long getCredit() {
        return credit;
    }

    public void setCredit(long credit) {
        this.credit = credit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void increaseCredit(Long extraCredit) {
        this.credit += extraCredit;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}