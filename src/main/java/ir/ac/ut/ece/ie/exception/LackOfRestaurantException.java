package ir.ac.ut.ece.ie.exception;

public class LackOfRestaurantException extends RuntimeException  {
    public LackOfRestaurantException(String message) {
        super(message);
    }
}
