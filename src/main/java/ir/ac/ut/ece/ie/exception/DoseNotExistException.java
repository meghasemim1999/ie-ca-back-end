package ir.ac.ut.ece.ie.exception;

public class DoseNotExistException extends Exception {
    public DoseNotExistException(String message) {
        super(message);
    }
}
