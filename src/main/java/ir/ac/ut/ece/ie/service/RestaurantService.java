package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.domain.*;
import ir.ac.ut.ece.ie.exception.FurtherThanAllowedException;
import ir.ac.ut.ece.ie.exception.LackOfAnyRestaurantException;
import ir.ac.ut.ece.ie.exception.LackOfRestaurantException;
import ir.ac.ut.ece.ie.exception.NoLoggedInUser;
import ir.ac.ut.ece.ie.repository.RestaurantRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RestaurantService {
    @RequestMapping(value = "/Restaurant", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getRestaurant(@RequestParam("id") String id, @RequestAttribute("user") User loggedInUser) {
        RestaurantViewModel restaurantInfo = null;
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            restaurantInfo = LoghmehCore.getInstance().getRestaurant(id);
        } catch (LackOfRestaurantException | FurtherThanAllowedException | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, restaurantInfo, null);
    }

    @RequestMapping(value = "/Restaurant/All", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getRestaurantsList(@RequestAttribute("user") User loggedInUser) {
        List<RestaurantListItemViewModel> restaurants = null;
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            restaurants = LoghmehCore.getInstance().getRestaurants();
        } catch (LackOfAnyRestaurantException | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, restaurants, null);
    }


    @RequestMapping(value = "/Restaurant/FoodParty", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult foodParty() throws SQLException {
        List<RestaurantInfo> restaurants;
        List<DiscountedFood> foodPartyFood = new ArrayList<>();
        restaurants = RestaurantRepository.getInstance().getAll();
        for (int i = 0; i < restaurants.size(); i++) {
            RestaurantInfo restaurant = restaurants.get(i);
            if (restaurant.hasFoodParty()){
                System.out.printf("hi");
                foodPartyFood.addAll(restaurant.getFoodPartyMenu());
            }else
                continue;

        }
        return new ServiceResult(true, foodPartyFood, null);
    }
}