package ir.ac.ut.ece.ie.runnable;

import ir.ac.ut.ece.ie.domain.Delivery;
import ir.ac.ut.ece.ie.domain.Order;
import ir.ac.ut.ece.ie.repository.DeliveryRepository;
import ir.ac.ut.ece.ie.repository.OrderRepository;

import java.sql.SQLException;
import java.util.List;

public class AssignDeliveryToOrders implements Runnable {

    @Override
    public void run() {
        try {
            List<Order> orders = OrderRepository.getInstance().getAll();
            for (int i = 0; i < orders.size(); i++) {
                if (orders.get(i).getOrderState() == Order.OrderState.SEARCHING_FOR_DELIVERY) {
                    Delivery assignableDelivery = DeliveryRepository.getInstance().searchForBestDelivery(orders.get(i).getOrderOwner().getLocation(),
                            orders.get(i).getOrderRestaurant().getLocation());
                    if (assignableDelivery == null)
                        continue;
                    assignableDelivery.assignOrder(orders.get(i));
                    DeliveryRepository.getInstance().addOrUpdate(assignableDelivery);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
