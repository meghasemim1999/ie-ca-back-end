package ir.ac.ut.ece.ie.exception;

public class FoodPriceNotFound extends Exception {
    public FoodPriceNotFound(String message)
    {
        super(message);
    }
}
