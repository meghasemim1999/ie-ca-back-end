package ir.ac.ut.ece.ie.domain;

public class OrderedFoodInfo {
    private String foodName;
    private String restaurantName;
    private int ordersNumber;
    private int price;
    private boolean isRegular;

    public OrderedFoodInfo(String foodName, String restaurantName, Integer ordersNumber, Integer price, boolean isRegular){
        this.foodName = foodName;
        this.restaurantName = restaurantName;
        this.ordersNumber = ordersNumber;
        this.price = price;
        this.isRegular = isRegular;
    }
    public OrderedFoodInfo(){
        ordersNumber = 0;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getOrdersNumber() {
        return ordersNumber;
    }

    public void setOrdersNumber(int ordersNumber) {
        this.ordersNumber = ordersNumber;
    }

    public boolean isRegular() {
        return isRegular;
    }

    public void setRegular(boolean regular) {
        isRegular = regular;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
}
