package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.domain.Location;

public class RepositoryUtilities {
    static public Location convertLocationStringToLocation(String locationStr) {
        int underline_index = locationStr.indexOf('_');
        String x_str = locationStr.substring(0, underline_index );
        String y_str = locationStr.substring(underline_index + 1);
        int x = Integer.parseInt(x_str);
        int y = Integer.parseInt(y_str);
        return new Location(x, y);
    }
}
