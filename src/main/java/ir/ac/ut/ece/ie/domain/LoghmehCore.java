package ir.ac.ut.ece.ie.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.exception.*;
import ir.ac.ut.ece.ie.repository.*;
import ir.ac.ut.ece.ie.runnable.AssignDeliveryToOrders;
import ir.ac.ut.ece.ie.service.RestaurantListItemViewModel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import static java.lang.Math.pow;

public class LoghmehCore {
    private static LoghmehCore instance;
    private ObjectMapper mapper;
    private List<OrderedFoodInfo> cart;
    private String orderOnGoingRestaurant;
    private User loggedInUser;

    public String getOrderOnGoingRestaurant(){
        return orderOnGoingRestaurant;
    }

    private static final int MAXIMUM_DISTANCE_ALLOWED = 170;

    private LoghmehCore() throws SQLException {
        mapper = new ObjectMapper();
        cart = new ArrayList<>();
        orderOnGoingRestaurant = "";
    }

    public static LoghmehCore getInstance() throws SQLException {
        if(instance == null)
            instance = new LoghmehCore();

        return instance;
    }

    public User getUserProfile() throws LackOfUserException, SQLException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        return loggedInUser;
    }

    public void increaseCredit(Long extraCredit) throws SQLException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        loggedInUser.increaseCredit(extraCredit);
        UserRepository.getInstance().addOrUpdate(loggedInUser);
    }

    public void addRestaurant(String restaurantInfoString) throws IOException, DuplicateRestaurantException, SQLException {
        RestaurantInfo newRestaurant;
        newRestaurant = mapper.readValue(restaurantInfoString, RestaurantInfo.class);
        newRestaurant.setRegularMenuFromMenu();
        RestaurantRepository.getInstance().addOrUpdate(newRestaurant);
    }

    public void addFood(String foodInfoString) throws IOException, DuplicateFoodException, LackOfRestaurantException, SQLException {
        FoodInfo newFood;
        newFood = mapper.readValue(foodInfoString, FoodInfo.class);
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
        {
            if(restaurant.getName().equals(newFood.getRestaurantName()))
            {
                for(RestaurantFoodInfo restaurantFoodInfo : restaurant.getRegularMenu())
                    if(restaurantFoodInfo.getName().equals(newFood.getName()))
                        throw new DuplicateFoodException("Food with name " + newFood.getName() + " already exists.");
                RestaurantFoodInfo food = convertFoodToRestaurantFood(newFood);
                restaurant.getRegularMenu().add(food);
                RestaurantRepository.getInstance().addOrUpdate(restaurant); /// isn't it by reference? do we really need update method?
                return;
            }
        }
        throw new LackOfRestaurantException("Restaurant with name: " + newFood.restaurantName + " does not exist.");
    }

    public RestaurantFoodInfo convertFoodToRestaurantFood(FoodInfo food){
        RestaurantFoodInfo restaurantFood = new RestaurantFoodInfo();
        restaurantFood.setName(food.getName());
        restaurantFood.setDescription(food.getDescription());
        restaurantFood.setPopularity(food.getPopularity());
        restaurantFood.setPrice(food.getPrice());
        return restaurantFood;
    }

    public List<RestaurantListItemViewModel> getRestaurants() throws LackOfAnyRestaurantException, SQLException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        List<RestaurantInfo> restaurants = RestaurantRepository.getInstance().getAll();
        if (restaurants.isEmpty())
            throw new LackOfAnyRestaurantException("There is no restaurant in Loghme right now.");
        List<RestaurantListItemViewModel> restaurantViewModels = new ArrayList<>();
        for (int i = 0; i < restaurants.size(); i++) {
            RestaurantInfo restaurant = restaurants.get(i);
            if (Math.sqrt(pow(restaurant.getLocation().getX() -
                   loggedInUser.getLocation().getX(), 2)
                    + pow(restaurant.getLocation().getY() -
                    loggedInUser.getLocation().getY(), 2)) > MAXIMUM_DISTANCE_ALLOWED)
                continue;
            RestaurantListItemViewModel model = new RestaurantListItemViewModel();
            model.id = restaurant.getId();
            model.name = restaurant.getName();
            model.logo = restaurant.getLogo();
            restaurantViewModels.add(model);
        }
        return restaurantViewModels;
    }

    public RestaurantViewModel getRestaurant(String id) throws LackOfRestaurantException, FurtherThanAllowedException, SQLException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        RestaurantInfo restaurant = RestaurantRepository.getInstance().getById(id);
        if(Math.sqrt(pow(restaurant.getLocation().getX() - loggedInUser.getLocation().getX(), 2)
                + pow(restaurant.getLocation().getY() - loggedInUser.getLocation().getY(), 2)) <= MAXIMUM_DISTANCE_ALLOWED)
            {
                RestaurantViewModel model = new RestaurantViewModel(restaurant.getId(),
                        restaurant.getLogo(),
                        restaurant.getName(),
                        restaurant.getLocation(),
                        restaurant.getRegularMenu(),
                        restaurant.getFoodPartyMenu());
                return model;
            }
        else
            throw new FurtherThanAllowedException("Restaurant distance is further than " + MAXIMUM_DISTANCE_ALLOWED + ".");
    }

    public String getFood(String intendedFoodInfoString) throws IOException, LackOfFoodException, LackOfRestaurantException, SQLException {
        IntendedFoodInfo intendedFood;
        intendedFood = mapper.readValue(intendedFoodInfoString, IntendedFoodInfo.class);
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
            if (restaurant.getName().equals(intendedFood.getRestaurantName())) {
                for (RestaurantFoodInfo food : restaurant.getRegularMenu())
                    if (food.getName().equals((intendedFood.getFoodName())))
                        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(food) + "\n";
                throw new LackOfFoodException("There is no food with name " + intendedFood.getFoodName() + ".");
            }
        throw new LackOfRestaurantException("Restaurant with name: " + intendedFood.getRestaurantName() + " does not exist.");
    }

    public static TreeMap<String, Float> sortMapByValue(HashMap<String, Float> map){
        Comparator<String> comparator = new ValueComparator(map);
        TreeMap<String, Float> result = new TreeMap<>(comparator);
        result.putAll(map);
        return result;
    }

    public String getRecommendedRestaurants() throws LackOfAnyRestaurantException, SQLException {
        HashMap<String, Float> restaurantPopulation = new HashMap<>();
        StringBuilder recommendedRestaurants = new StringBuilder();
        if (RestaurantRepository.getInstance().getAll().isEmpty())
            throw new LackOfAnyRestaurantException("There is no restaurant in Loghme right now.");
        for (RestaurantInfo restaurant : RestaurantRepository.getInstance().getAll())
            restaurantPopulation.put(restaurant.getName(), calculateRestaurantPopulation(restaurant));
        TreeMap<String, Float> sortedRestaurantPopulation = sortMapByValue(restaurantPopulation);
        int numOfRecommendedRestaurant = 0;
        for (Map.Entry<String, Float> entry : sortedRestaurantPopulation.entrySet())
        {
            recommendedRestaurants.append(entry.getKey()).append("\n");
            numOfRecommendedRestaurant++;
            if (numOfRecommendedRestaurant == 3)
                return recommendedRestaurants.toString();
        }
        return recommendedRestaurants.toString();
    }

    public Float calculateRestaurantPopulation(RestaurantInfo restaurant) throws SQLException {
        float averageFoodsPopulation = 0;
        float restaurantPopulation;
        for(RestaurantFoodInfo food : restaurant.getRegularMenu()){
            averageFoodsPopulation = food.getPopularity() + averageFoodsPopulation;
        }
        averageFoodsPopulation = averageFoodsPopulation / restaurant.getRegularMenu().size();
        restaurantPopulation = (float) (averageFoodsPopulation/ Math.sqrt(pow(restaurant.getLocation().getX() - loggedInUser.getLocation().getX(), 2)
                + pow(restaurant.getLocation().getY() - loggedInUser.getLocation().getY(), 2)));
        return  restaurantPopulation;
    }

    public void addToCart(IntendedFoodInfo intendedFoodInfo) throws UnfinishedOrderException, LackOfFoodException, LackOfRestaurantException, DoseNotExistException, FoodPriceNotFound, SQLException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        int foodPrice = -1;
        if( !orderOnGoingRestaurant.equals("") && !orderOnGoingRestaurant.equals(intendedFoodInfo.getRestaurantName()))
            throw new UnfinishedOrderException("Please finnish your ongoing order first.");
        boolean orderStatus = false;
        for (OrderedFoodInfo orderedFoodInfo : cart) {
            if(orderedFoodInfo.getFoodName().equals(intendedFoodInfo.getFoodName()) && (orderedFoodInfo.isRegular() == intendedFoodInfo.isRegular()) && orderedFoodInfo.isRegular()){
                orderedFoodInfo.setOrdersNumber(orderedFoodInfo.getOrdersNumber() + 1);
                return;
            }
        }

        List<RestaurantInfo> restaurants = RestaurantRepository.getInstance().getAll();

        for (int i = 0; i < restaurants.size(); i++)
        {
            if(restaurants.get(i).getName().equals(intendedFoodInfo.getRestaurantName())) {
                if (!intendedFoodInfo.isRegular()) {
                    List<DiscountedFood> foodPartyMenu = restaurants.get(i).getFoodPartyMenu();
                    for (int i1 = 0; i1 < foodPartyMenu.size(); i1++) {
                        DiscountedFood food = foodPartyMenu.get(i1);
                        if (food.getName().equals(intendedFoodInfo.getFoodName()))
                            if (food.getCount() > 0) {
                                food.decreaseCount();
                                for (OrderedFoodInfo orderedFoodInfo : cart) {
                                    if(orderedFoodInfo.getFoodName().equals(intendedFoodInfo.getFoodName()) && (orderedFoodInfo.isRegular() == intendedFoodInfo.isRegular()) && !orderedFoodInfo.isRegular()){
                                        orderedFoodInfo.setOrdersNumber(orderedFoodInfo.getOrdersNumber() + 1);
                                        return;
                                    }
                                }
                                orderStatus = true;
                                foodPrice = food.getPrice();
                                break;
                            } else
                                throw new DoseNotExistException("There is no " + food.getName() + " left.");
                    }
                }else{
                    for (RestaurantFoodInfo food : restaurants.get(i).getRegularMenu())
                        if (food.getName().equals((intendedFoodInfo.getFoodName()))) {
                            orderStatus = true;
                            foodPrice = food.getPrice();
                            break;
                        }
                }
                if (orderStatus) {
                    if(foodPrice == -1)
                        throw new FoodPriceNotFound("Food price not found and it is not added to cart.");
                    OrderedFoodInfo orderedFoodInfo = new OrderedFoodInfo();
                    orderedFoodInfo.setFoodName(intendedFoodInfo.getFoodName());
                    orderedFoodInfo.setRegular(intendedFoodInfo.isRegular());
                    orderedFoodInfo.setOrdersNumber(1);
                    orderedFoodInfo.setRestaurantName(intendedFoodInfo.getRestaurantName());
                    orderedFoodInfo.setPrice(foodPrice);
                    orderOnGoingRestaurant = intendedFoodInfo.getRestaurantName();
                    cart.add(orderedFoodInfo);
                    return;
                }
                throw new LackOfFoodException("Food with name " + intendedFoodInfo.getFoodName() + " does not exist.");
            }
        }
        throw new LackOfRestaurantException("Restaurant with name: " + intendedFoodInfo.getRestaurantName() + " does not exist.");
    }

    public void removeFromCart(IntendedFoodInfo intendedFoodInfo) throws LackOfFoodException, LackOfRestaurantException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        for (int i = 0 ; i < cart.size() ; i++ )
        {
            OrderedFoodInfo orderedFoodInfo = cart.get(i);
            if(orderedFoodInfo.getFoodName().equals(intendedFoodInfo.getFoodName()) && intendedFoodInfo.getRestaurantName().equals(orderOnGoingRestaurant))
            {
                if(orderedFoodInfo.getOrdersNumber() == 1)
                    cart.remove(i);
                else
                {
                    orderedFoodInfo.setOrdersNumber(orderedFoodInfo.getOrdersNumber() - 1);
                    cart.remove(i);
                    cart.add(orderedFoodInfo);
                }
                if (cart.size() == 0)
                    orderOnGoingRestaurant = "";
                return;
            }
        }
        throw new LackOfFoodException("Food does not Exist in restaurant.");
    }

    public List<OrderedFoodInfo> getCart() throws EmptyCartException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        if(cart.isEmpty())
            throw new EmptyCartException("Cart is empty.");
        return cart;
    }

    public void finalizeOrder() throws EmptyCartException, NotEnoughCreditException, LackOfFoodException, LackOfRestaurantException, SQLException, NoLoggedInUser {
        if(loggedInUser == null)
            throw new NoLoggedInUser("Please Login First, No user is Logged in right now.");
        if(cart.isEmpty())
            throw new EmptyCartException("Cart is empty.");
        int cartCost = calculateCartCost();
        if (!checkEnoughCredit(cartCost))
            throw new NotEnoughCreditException("User's credit is not enough.");
        loggedInUser.setCredit(loggedInUser.getCredit() - cartCost);
        UserRepository.getInstance().addOrUpdate(loggedInUser);
        List<OrderedFoodInfo> orderedFoods = new ArrayList<>(cart);
        RestaurantInfo orderRestaurant = RestaurantRepository.getInstance().getByName(orderOnGoingRestaurant);
        RestaurantViewModel model = new RestaurantViewModel(orderRestaurant.getId(),
                orderRestaurant.getLogo(),
                orderRestaurant.getName(),
                orderRestaurant.getLocation(),
                orderRestaurant.getRegularMenu(),
                orderRestaurant.getFoodPartyMenu()
                );
        Order newOrder = new Order(orderedFoods, loggedInUser, model);
        OrderRepository.getInstance().addOrUpdate(newOrder);
        (new AssignDeliveryToOrders()).run();
        cart.removeAll(cart);
        orderOnGoingRestaurant = "";
    }

    private int calculateCartCost() throws LackOfFoodException, LackOfRestaurantException, SQLException {
        int cost = 0;
        for (OrderedFoodInfo orderedFoodInfo : cart)
            cost += orderedFoodInfo.getOrdersNumber() * findFoodPrice(orderedFoodInfo);
        return cost;
    }

    private boolean checkEnoughCredit(int cost) throws SQLException {
        return loggedInUser.getCredit() >= cost;
    }

    private int findFoodPrice(OrderedFoodInfo orderedFood) throws LackOfFoodException, LackOfRestaurantException, SQLException {
        if (orderedFood.isRegular()) {
            List<RestaurantFoodInfo>regularMenu = RestaurantRepository.getInstance().getByName(orderOnGoingRestaurant).getRegularMenu();
            for (RestaurantFoodInfo food : regularMenu){
                if(food.getName().equals(orderedFood.getFoodName()))
                    return food.getPrice();
            }
        } else {
            List<DiscountedFood> foodPartyMenu = RestaurantRepository.getInstance().getByName(orderOnGoingRestaurant).getFoodPartyMenu();
            for (DiscountedFood food : foodPartyMenu) {
                System.out.println(food.getName());
                System.out.println(orderedFood.getFoodName());
                if (food.getName().equals(orderedFood.getFoodName()))
                    return food.getPrice();
            }
        }
        throw new LackOfFoodException("Food with name " + orderedFood.getFoodName() + " does not exist.");
    }

    public void setLoggedInUser(User loggedInUser) throws SQLException {
        if(loggedInUser != null)
            UserRepository.getInstance().addOrUpdate(loggedInUser);
        this.loggedInUser = loggedInUser;
    }
}
