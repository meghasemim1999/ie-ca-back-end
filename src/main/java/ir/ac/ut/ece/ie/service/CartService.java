package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.domain.IntendedFoodInfo;
import ir.ac.ut.ece.ie.domain.LoghmehCore;
import ir.ac.ut.ece.ie.domain.OrderedFoodInfo;
import ir.ac.ut.ece.ie.domain.User;
import ir.ac.ut.ece.ie.exception.*;
import ir.ac.ut.ece.ie.service.requestBody.IntendedFoodRequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
public class CartService {
    @RequestMapping( value = "/Cart", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getCart(@RequestAttribute("user") User loggedInUser) {
        List<OrderedFoodInfo> cart;
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            cart = LoghmehCore.getInstance().getCart();
        } catch (EmptyCartException | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, cart, null);
    }

    @RequestMapping( value ="/Cart", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult addToCart(@RequestBody IntendedFoodRequestBody body, @RequestAttribute("user") User loggedInUser) {
        IntendedFoodInfo intendedFoodInfo = new IntendedFoodInfo(body.getRestaurantName(), body.getFoodName(), body.getIsRegular().equals("true"));
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            LoghmehCore.getInstance().addToCart(intendedFoodInfo);
        } catch (UnfinishedOrderException | LackOfFoodException | DoseNotExistException | LackOfRestaurantException | FoodPriceNotFound | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, null, null);
    }

    @RequestMapping( value ="/Cart", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult removeFromCart(@RequestBody IntendedFoodRequestBody body, @RequestAttribute("user") User loggedInUser) {
        IntendedFoodInfo intendedFoodInfo = new IntendedFoodInfo(body.getRestaurantName(), body.getFoodName(), body.getIsRegular().equals("true"));
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            LoghmehCore.getInstance().removeFromCart(intendedFoodInfo);
        } catch (LackOfFoodException | LackOfRestaurantException | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, null, null);
    }

    @RequestMapping( value = "/Cart/Number", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getNumOfCartFood(@RequestAttribute("user") User loggedInUser) {
        List<OrderedFoodInfo> cart;
        int total = 0;
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            cart = LoghmehCore.getInstance().getCart();

            for (int i = 0; i< cart.size();i++){
                total+= cart.get(i).getOrdersNumber();
            }
        } catch (EmptyCartException | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, total, null);
    }
}
