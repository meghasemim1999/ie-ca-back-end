package ir.ac.ut.ece.ie.domain;

import java.util.List;

public class RestaurantViewModel {
    private String id;
    private String logo;
    private String name;
    private Location location;
    private List<RestaurantFoodInfo> regularMenu;
    private List<DiscountedFood> foodPartyMenu;

    public RestaurantViewModel(String id,
                               String logo,
                               String name,
                               Location location,
                               List<RestaurantFoodInfo> regularMenu,
                               List<DiscountedFood> foodPartyMenu) {
        this.id = id;
        this.logo = logo;
        this.name = name;
        this.location = location;
        this.regularMenu = regularMenu;
        this.foodPartyMenu = foodPartyMenu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<RestaurantFoodInfo> getRegularMenu() {
        return regularMenu;
    }

    public void setRegularMenu(List<RestaurantFoodInfo> regularMenu) {
        this.regularMenu = regularMenu;
    }

    public List<DiscountedFood> getFoodPartyMenu() {
        return foodPartyMenu;
    }

    public void setFoodPartyMenu(List<DiscountedFood> foodPartyMenu) {
        this.foodPartyMenu = foodPartyMenu;
    }
}
