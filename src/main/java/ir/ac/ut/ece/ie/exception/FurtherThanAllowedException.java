package ir.ac.ut.ece.ie.exception;

public class FurtherThanAllowedException extends RuntimeException  {
    public FurtherThanAllowedException(String message) {
        super(message);
    }
}
