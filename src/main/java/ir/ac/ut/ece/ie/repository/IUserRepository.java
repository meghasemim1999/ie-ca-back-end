package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.domain.User;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User, Integer> {
    String getCountStatement();
    
    Integer count() throws SQLException;
}
