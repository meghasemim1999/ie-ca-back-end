package ir.ac.ut.ece.ie.exception;

public class LackOfFoodException extends Exception {
    public LackOfFoodException(String message) {
        super(message);
    }
}
