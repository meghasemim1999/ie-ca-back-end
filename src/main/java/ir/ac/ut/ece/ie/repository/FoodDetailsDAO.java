package ir.ac.ut.ece.ie.repository;

public class FoodDetailsDAO {
    private String foodName;
    private Integer foodPrice;

    public FoodDetailsDAO(String foodName, Integer foodPrice) {
        this.foodName = foodName;
        this.foodPrice = foodPrice;
    }

    public String getFoodName() {
        return foodName;
    }

    public Integer getFoodPrice() {
        return foodPrice;
    }
}
