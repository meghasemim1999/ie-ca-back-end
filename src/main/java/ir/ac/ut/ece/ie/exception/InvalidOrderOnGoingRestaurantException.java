package ir.ac.ut.ece.ie.exception;

public class InvalidOrderOnGoingRestaurantException extends Exception {
    public InvalidOrderOnGoingRestaurantException(String message) {
        super(message);
    }
}
