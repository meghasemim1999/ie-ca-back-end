package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.domain.LoghmehCore;
import ir.ac.ut.ece.ie.domain.Order;
import ir.ac.ut.ece.ie.domain.User;
import ir.ac.ut.ece.ie.exception.*;
import ir.ac.ut.ece.ie.repository.OrderRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
public class OrderService {
    @RequestMapping(value = "/Order", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getOrder(@RequestAttribute("user") User loggedInUser) throws SQLException, LackOfOrderException {
        List<Order> orders = null;
        orders = OrderRepository.getInstance().getUserOrders(loggedInUser.getId());
        return new ServiceResult(true, orders, null);
    }

    @RequestMapping(value = "/Order", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult finalizeCart(@RequestAttribute("user") User loggedInUser) {
        try {
            LoghmehCore.getInstance().setLoggedInUser(loggedInUser);
            LoghmehCore.getInstance().finalizeOrder();
        } catch (EmptyCartException | NotEnoughCreditException | LackOfFoodException | SQLException | NoLoggedInUser exception) {
            return new ServiceResult(false, null, exception);
        }
        return new ServiceResult(true, null, null);
    }
}
