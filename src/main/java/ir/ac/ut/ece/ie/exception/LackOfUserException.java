package ir.ac.ut.ece.ie.exception;

public class LackOfUserException extends Exception {
    public LackOfUserException(String message) {
        super(message);
    }
}
