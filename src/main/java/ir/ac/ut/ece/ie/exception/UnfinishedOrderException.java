package ir.ac.ut.ece.ie.exception;

public class UnfinishedOrderException extends Exception {
    public UnfinishedOrderException(String message) {
        super(message);
    }
}
