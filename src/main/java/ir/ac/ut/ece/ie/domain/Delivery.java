package ir.ac.ut.ece.ie.domain;

import ir.ac.ut.ece.ie.repository.OrderRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.pow;

public class Delivery {
    private String id;
    private float velocity;
    private Location location;
    private List<Order> orders;

    public Delivery(){
        orders = new ArrayList<>();
    }

    private class DeliverOrder implements Runnable
    {
        Order order;

        DeliverOrder(Order order){
            this.order = order;
        }

        @Override
        public void run() {
            try {
                try {
                    Thread.sleep((long) neededTimeToDeliver(order.getOrderOwner().getLocation(), order.getOrderRestaurant().getLocation()) * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < orders.size(); i++) {
                    if (orders.get(i).getId() == order.getId())
                        orders.remove(i);
                }
                order.setOrderState(Order.OrderState.DELIVERED);
                OrderRepository.getInstance().addOrUpdate(order);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void assignOrder(Order order) throws SQLException {
        orders.add(order);
        order.setOrderState(Order.OrderState.DELIVERY_ON_THE_WAY);
        OrderRepository.getInstance().addOrUpdate(order);
        Thread deliverThread = new Thread(new DeliverOrder(order));
        deliverThread.start();
//        Scheduler.getScheduler().schedule(new DeliverOrder(order),
//                (long) neededTimeToDeliver(order.getOrderOwner().getLocation(), order.getOrderRestaurant().getLocation()),
//                TimeUnit.SECONDS);
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public double neededTimeToDeliver(Location userLocation, Location restaurantLocation){
        double distance = Math.sqrt(pow(this.location.getX() - restaurantLocation.getX(), 2) + pow(this.location.getY() - restaurantLocation.getY(), 2)) + Math.sqrt(pow(this.location.getX() - userLocation.getX(), 2) + pow(this.location.getY() - userLocation.getY(), 2));
        return distance/this.velocity;
    }
}
