package ir.ac.ut.ece.ie.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.domain.User;
import ir.ac.ut.ece.ie.repository.UserRepository;
import ir.ac.ut.ece.ie.service.requestBody.GoogleLoginRequestBody;
import ir.ac.ut.ece.ie.service.requestBody.LoginRequestBody;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.io.IOException;
import java.sql.SQLException;

@RestController
public class AuthService {
    @RequestMapping(value = "/Auth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResult> login(@RequestBody LoginRequestBody body) {
        ResponseEntity<ServiceResult> response = null;
        try {
            User user = UserRepository.getInstance().getByEmail(body.getEmail());
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if(user == null || !encoder.matches(body.getPassword(), user.getPassword()))
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            String token = JwtToken.create(user.getId());
            return new ResponseEntity<>(new ServiceResult(true, token, null), HttpStatus.ACCEPTED);
        } catch (SQLException exception) {
            return new ResponseEntity<>(new ServiceResult(false, null, exception), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/Auth/Google", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResult> googleLogin(@RequestBody GoogleLoginRequestBody body) {
        ResponseEntity<ServiceResult> response = null;
        try {
            HttpResponse<String> tokenVerifierResponse = Unirest.get("https://oauth2.googleapis.com/tokeninfo?id_token=" + body.getToken())
                    .header("accept", "application/json")
                    .asString();
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readValue(tokenVerifierResponse.getBody(), JsonNode.class);
            User user = UserRepository.getInstance().getByEmail(jsonNode.get("email").asText());
            String token;
            if( user == null)
            {
                user = new User(
                        -1,
                        jsonNode.get("given_name").asText(),
                        jsonNode.get("family_name").asText(),
                        "تعریف نشده",
                        jsonNode.get("email").asText(),
                        "undefined",
                        User.INITIAL_CREDIT,
                        User.INITIAL_LOCATION
                );
                UserRepository.getInstance().addOrUpdate(user);
                user = UserRepository.getInstance().getByEmail(jsonNode.get("email").asText());
            }
            token = JwtToken.create(user.getId());
            return new ResponseEntity<>(new ServiceResult(true, token, null), HttpStatus.ACCEPTED);
        } catch (SQLException | IOException exception) {
            return new ResponseEntity<>(new ServiceResult(false, null, exception), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}