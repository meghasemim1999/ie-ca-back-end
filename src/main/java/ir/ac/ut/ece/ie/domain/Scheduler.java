package ir.ac.ut.ece.ie.domain;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Scheduler {
    private static ScheduledExecutorService scheduler;

    public static ScheduledExecutorService getScheduler(){
        if(scheduler == null)
            scheduler = Executors.newScheduledThreadPool(5);

        return scheduler;
    }
}