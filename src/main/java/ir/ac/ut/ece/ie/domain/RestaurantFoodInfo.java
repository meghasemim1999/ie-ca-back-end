package ir.ac.ut.ece.ie.domain;

public class RestaurantFoodInfo {
    private String name;
    private String restaurantName;
    private String description;
    private float popularity;
    private int price;
    private String image;
    private boolean doesExist;

    public RestaurantFoodInfo(String name, String restaurantName, String description, float popularity, String image, int price)
    {
        this.name = name;
        this.restaurantName = restaurantName;
        this.description = description;
        this.popularity = popularity;
        this.image = image;
        this.price = price;
        this.doesExist = true;
    }

    public RestaurantFoodInfo()  {
        doesExist = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPopularity() {
        return popularity;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean getDoesExist() {
        return doesExist;
    }

    public void setDoesExist(boolean doesExist) {
        this.doesExist = doesExist;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
}
